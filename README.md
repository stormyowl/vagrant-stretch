# Vagrant setup on Debian 9.x

## Install Vagrant and LXC

```console
$ sudo apt-get install vagrant lxc redir
$ vagrant -v
Vagrant 1.9.1
$ vagrant plugin install vagrant-lxc
Installing the 'vagrant-lxc' plugin. This can take a few minutes...
Fetching: vagrant-lxc-1.4.3.gem (100%)
Installed the plugin 'vagrant-lxc (1.4.3)'!
$ vagrant plugin list
vagrant-lxc (1.4.3)
```

## Configure LXC networking

Make sure that `/etc/default/lxc-net` contains only the following lines.

```
LXC_BRIDGE="lxcbr0"
LXC_ADDR="10.6.1.1"
LXC_NETMASK="255.255.255.0"
LXC_NETWORK="10.6.1.0/24"
LXC_DHCP_RANGE="10.6.1.200,10.6.1.254"
LXC_DHCP_MAX="253"
USE_LXC_BRIDGE="true"
```

Reboot your mashine.

## Install ansible and provision Vagrant box

```console
$ mkvirtualenv ansible -p python3
$ pip install -r requirements.txt
$ vagrant up
```

## Troubleshooting

### LXC issues

The `lxc` package doesn't seem to be installed.

Run `which vagrant-lxc-wrapper` and remove the file, after that run

```console
vagrant lxc sudoers
```

which will recreate the `vagrant-lxc-wrapper` file.

### DHCP issues

There was an error executing `lxc-info`, you've probably got some custom `iptables` rules or VPN that is blocking outgoing DHCP traffic.

Check `iptables` by running `sudo iptables -L`, if that's the issue add custom rule to allow traffic to `lxc-bridge` network.

```console
$ sudo iptables -I OUTPUT -s 10.6.1.0/20 -j ACCEPT
```

# Running playbook against local Vagrant mashine

```console
ansible-playbook -i .vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory path/to/playbook.yml
```
