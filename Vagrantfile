# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  config.vm.box = "debian/stretch64"
  config.ssh.forward_agent = true

  lxc_version = `lxc-create --version || echo 0.0.0`
  config.vm.provider :lxc do |lxc, override|
      if Gem::Version.new(lxc_version) >= Gem::Version.new('3.0.0')
          lxc.customize 'net.0.type', 'veth'
          lxc.customize 'net.0.link', 'lxcbr0'
          lxc.customize 'apparmor.profile', 'unconfined'
      else
          lxc.customize 'network.type', 'veth'
          lxc.customize 'network.link', 'lxcbr0'
          lxc.customize 'aa_profile', 'unconfined'
          lxc.backingstore = 'dir'
      end
  end

  config.vm.network "private_network", ip: "192.168.33.20", lxc__bridge_name: 'lxcbr1'
  config.vm.network "forwarded_port", guest: 80, host: 8080

  config.vm.provision "ansible" do |ansible|
      ansible.playbook = "playbooks/provision.yml"
  end

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"
end
